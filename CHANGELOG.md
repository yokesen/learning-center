## Luma Laravel v1.0.1
> Oct 28, 2020

```
Changes
- Add stylesheet preloading fallback for Firefox and other browsers without support for link[rel=preload]
- Fixed Browser Support via .babelrc and core-js@3 (last 2 versions of every major browser and IE >= 11)
```

## Luma Laravel v1.0.0
> Oct 26, 2020

```
- Initial release
```
